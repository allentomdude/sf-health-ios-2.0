//
//  FilterBarView.swift
//  SF Health 2
//
//  Created by Allen Tom on 5/12/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import UIKit
protocol filterBarViewDelegate {
    func scoreFilterUpdated()
    func distanceFilterUpdated(distance:Double)
}

class FilterBarView: UIView {
    
    // custom xib loading code: https://gist.github.com/bwhiteley/049e4bede49e71a6d2e2
    // remember to add code to break out of loop
    
    
    
    var view: UIView!
    var delegate:filterBarViewDelegate?
    
    // custom xib loading code from
    // http://supereasyapps.com/blog/2014/12/15/create-an-ibdesignable-uiview-subclass-with-code-from-an-xib-file-in-xcode-6
    // be sure to only set the file's owner to the Class, do not set the View, otherwide
    // will result in an infinite loop
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    
    init() {
        super.init(frame: CGRect())
        xibSetup()
    }
    
    
    // need to resize the filter buttons when the screen is rotated
    func didRotate(size:CGSize) {
        print("filterBarView: didRotate UIScreen.main.bounds.width: \(UIScreen.main.bounds.width)")
        let scoreButtonWidth = (size.width - 179) / 3
        
        highButtonWidthConstraint.constant = scoreButtonWidth
        okButtonWidthConstraint.constant = scoreButtonWidth
        lowButtonWidthConstraint.constant = scoreButtonWidth

    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
        self.distanceView.layer.borderColor = UIColor.lightGray.cgColor
        self.distanceButton.layer.borderColor = UIColor.lightGray.cgColor
        self.mediumDistanceButton.layer.borderColor = UIColor.lightGray.cgColor
        self.distanceViewWidthConstraint.constant = 140
        
        highButton.layer.borderColor = UIColor.lightGray.cgColor
        okButton.layer.borderColor = UIColor.lightGray.cgColor
        lowButton.layer.borderColor = UIColor.lightGray.cgColor
        
        highButton.layer.borderWidth = 0.5
        okButton.layer.borderWidth = 0.5
        lowButton.layer.borderWidth = 0.5
        
        updateScoreButtons()
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = 1.0
        
        let scoreButtonWidth = (UIScreen.main.bounds.width - 179) / 3
        
        highButtonWidthConstraint.constant = scoreButtonWidth
        okButtonWidthConstraint.constant = scoreButtonWidth
        lowButtonWidthConstraint.constant = scoreButtonWidth
        
        
    }
    
    func loadViewFromNib() -> UIView {
        let view = Bundle.main.loadNibNamed("FilterBarView", owner: self, options: nil)?[0] as! UIView
        return view
    }
    
    var isDistanceExpanded = false
    
    
    @IBOutlet var distanceButton: UIButton!
    @IBOutlet var distanceView: UIView!
    @IBOutlet var distanceViewWidthConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var lowButton: UIButton!
    
    @IBOutlet var okButton: UIButton!
    
    @IBOutlet var highButton: UIButton!
    
    @IBOutlet var shortDistanceButton: UIButton!
    @IBOutlet var mediumDistanceButton: UIButton!
    @IBOutlet var longDistanceButton: UIButton!
    
    
    @IBOutlet var lowButtonWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var okButtonWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var highButtonWidthConstraint: NSLayoutConstraint!
    
    let lowButtonHighlightColor = UIColor(red: 243.0/255, green: 64.0/255.0, blue: 48.0/255, alpha: 1.0)
    let okButtonHighlightColor = UIColor(red: 246.0/255.0, green: 195.0/255.0, blue: 48.0/255.0, alpha: 1.0)
    let highButtonHighlightColor = UIColor(red: 62.0/255.0, green: 192.0/255.0, blue: 140.0/255, alpha: 1.0)
    
    
    func updateScoreButtons() {
        lowButton.backgroundColor = UIColor.white
        okButton.backgroundColor = UIColor.white
        highButton.backgroundColor = UIColor.white
        lowButton.setTitleColor(UIColor.black, for: .normal)
        okButton.setTitleColor(UIColor.black, for: .normal)
        highButton.setTitleColor(UIColor.black, for: .normal)
        
        if SFHealth.scoreFilters.contains(ScoreFilter.low) {
                lowButton.backgroundColor = lowButtonHighlightColor
                lowButton.setTitleColor(UIColor.white, for: .normal)
            }
            
            if SFHealth.scoreFilters.contains(ScoreFilter.ok) {
                okButton.backgroundColor = okButtonHighlightColor
                okButton.setTitleColor( UIColor(red: 0x4a/0xff, green: 0x4a/0xff, blue: 0x4a/0xff, alpha: 1.0), for: .normal)
            }
            
            if SFHealth.scoreFilters.contains(ScoreFilter.high) {
                highButton.backgroundColor = highButtonHighlightColor
                highButton.setTitleColor(UIColor.white, for: .normal)
            }
    }
    
    
    func expandDistanceView() {
        isDistanceExpanded = true
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 7.0, options: [], animations: {
            self.distanceViewWidthConstraint.constant = 290
            self.view.layoutIfNeeded()
        },
                       completion: nil )
        
        
        print("expandDistance")
    }
    
    func shrinkDistanceView() {
        isDistanceExpanded = false
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 7.0, options: [], animations: {
            self.distanceViewWidthConstraint.constant = 140
            self.view.layoutIfNeeded()
        },
                       completion: nil )
        
        print("shrinkDistanceView")
    }
    
    
    
    @IBAction func shortDistanceTapped(_ sender: Any) {
        print("shortDistanceTapped")
        distanceButton.setTitle("Distance 0.2 mi", for: .normal)
        shrinkDistanceView()
        if let delegate = delegate {
            delegate.distanceFilterUpdated(distance:640.0)
        }
    }
    
    
    @IBAction func mediumDistanceTapped(_ sender: Any) {
        print("mediumDistanceTapped")
        distanceButton.setTitle("Distance 0.5 mi", for: .normal)
        shrinkDistanceView()
        if let delegate = delegate {
            delegate.distanceFilterUpdated(distance:1600.0)
        }
    }
    
    
    @IBAction func longDistanceTapped(_ sender: Any) {
        print("longDistanceTapped")
        distanceButton.setTitle("Distance 1 mi", for: .normal)
        shrinkDistanceView()
        if let delegate = delegate {
            delegate.distanceFilterUpdated(distance:3200)
        }
    }
    
    @IBAction func distanceTapped(_ sender: Any) {
        print("distanceTapped")
        if isDistanceExpanded {
            shrinkDistanceView()
        }
        else {
            isDistanceExpanded = true
            expandDistanceView()
        }
    }
    
    
    @IBAction func highTapped(_ sender: Any) {
        print("highTapped")
        if SFHealth.scoreFilters.contains(ScoreFilter.high) {
            SFHealth.scoreFilters.remove(ScoreFilter.high)
        }
        else {
            SFHealth.scoreFilters.insert(ScoreFilter.high)
        }
        
        updateScoreButtons()
        
        if let delegate = delegate {
            delegate.scoreFilterUpdated()
        }
    }
    
    
    @IBAction func okTapped(_ sender: Any) {
        print("okTapped")
        if SFHealth.scoreFilters.contains(ScoreFilter.ok) {
            SFHealth.scoreFilters.remove(ScoreFilter.ok)
        }
        else {
            SFHealth.scoreFilters.insert(ScoreFilter.ok)
        }
        updateScoreButtons()
        
        if let delegate = delegate {
            delegate.scoreFilterUpdated()
        }
    }
    
    @IBAction func lowTapped(_ sender: Any) {
        print("lowTapped")
        if SFHealth.scoreFilters.contains(ScoreFilter.low) {
            SFHealth.scoreFilters.remove(ScoreFilter.low)
        }
        else {
            SFHealth.scoreFilters.insert(ScoreFilter.low)
        }
        updateScoreButtons()
        if let delegate = delegate {
            delegate.scoreFilterUpdated()
        }
    }
}
