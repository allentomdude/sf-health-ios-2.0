//
//  RestaurantReport.swift
//  SF Health 2
//
//  Created by Allen Tom on 5/18/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import UIKit

class RestaurantReportView: UIView {

    var view: UIView!
    
    
    @IBOutlet var scoreLabel: UILabel!
    
    @IBOutlet var dateLabel: UILabel!
    
    
    @IBOutlet var reportDetailsLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    init() {
        super.init(frame: CGRect())
        xibSetup()
    }
    
    func createParagraphAttribute() ->NSParagraphStyle {
        var paragraphStyle: NSMutableParagraphStyle
        paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.tabStops = [NSTextTab(textAlignment: .left, location: 5, options: NSDictionary() as! [NSTextTab.OptionKey : Any])]
        paragraphStyle.defaultTabInterval = 15
        paragraphStyle.firstLineHeadIndent = 0
        paragraphStyle.lineSpacing = 3
        paragraphStyle.paragraphSpacing = 13
        paragraphStyle.headIndent = 22 // adjust this for all lines after the first
        return paragraphStyle
    }

    
    
    func setup(report r: CombinedRestaurantReport) {
        
        let attributesDictionary = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15)]
        let fullAttrString = NSMutableAttributedString(string: "", attributes: attributesDictionary)

        
        for singleReport in r.descriptions {
            let formattedString = "•    \(singleReport)\n"
            let attrString = NSMutableAttributedString(string: formattedString)
            let paragraphStyle = createParagraphAttribute()
            attrString.addAttributes([NSAttributedStringKey.paragraphStyle: paragraphStyle], range: NSMakeRange(0, attrString.length))
            fullAttrString.append(attrString)
        }
        
        reportDetailsLabel.attributedText    = fullAttrString
        scoreLabel.text = "\(r.score)"
        scoreLabel.textColor = Restaurant.textLabelColor(score: r.score)
        scoreLabel.backgroundColor = Restaurant.labelColor(score: r.score)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM d, YYYY"
        
        dateLabel.text = dateFormatter.string(from: r.date)
        reportDetailsLabel.sizeToFit()

        // add a little extra padding to the bottom
        var  paddedFrame = reportDetailsLabel.frame
        paddedFrame.size.height += 10
        reportDetailsLabel.frame = paddedFrame
        
        reportDetailsLabel.layoutIfNeeded()

    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView {
        let view = Bundle.main.loadNibNamed("RestaurantReportView", owner: self, options: nil)?[0] as! UIView
        return view
    }
    

    

}
