//
//  RestaurantTableViewCell.swift
//  SF Health 2
//
//  Created by Allen Tom on 5/13/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import UIKit
import GooglePlaces

class RestaurantTableViewCell: UITableViewCell {

    
    @IBOutlet weak var restaurantNameLabel: UILabel!
    
    
    @IBOutlet weak var restaurantAddressLabel: UILabel!
    
    
    @IBOutlet weak var restaurantDistanceLabel: UILabel!
    

    @IBOutlet var restaurantScoreLabel: UILabel!
    
    
    @IBOutlet var restaurantImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        // don't turn the score label's background color clear
        let labelColor = restaurantScoreLabel.backgroundColor
        super.setSelected(selected, animated: animated)
        restaurantScoreLabel.backgroundColor = labelColor
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        // don't turn the score label's background color clear
        let labelColor = restaurantScoreLabel.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        restaurantScoreLabel.backgroundColor = labelColor
    }
    
    
    func configureCell(restaurant: RealmRestaurant) {
        self.restaurantNameLabel.text = restaurant.name
        self.restaurantAddressLabel.text = restaurant.address
        self.restaurantDistanceLabel.text = SFHealth.distanceFromUserString(restaurant: restaurant)
        self.restaurantScoreLabel.text = "\(restaurant.score)"

        self.restaurantScoreLabel.backgroundColor = SFHealth.scoreLabelColor(score: restaurant.score)
        self.restaurantScoreLabel.textColor = SFHealth.scoreTextLabelColor(score: restaurant.score)

        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 0xf2/0xff, green: 0xf2/0xff, blue: 0xf2/0xff, alpha: 1.0)
        self.selectedBackgroundView = backgroundView
        
        if restaurant.gplace_id != "" {
            self.loadFirstPhotoForPlace(placeID: restaurant.gplace_id)
        }
    }
    
}

//Google Places stuff
extension RestaurantTableViewCell {
    func loadFirstPhotoForPlace(placeID: String) {
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: placeID) { (photos, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                if let firstPhoto = photos?.results.first {
                    self.loadImageForMetadata(photoMetadata: firstPhoto)
                }
            }
        }
    }
    
    func loadImageForMetadata(photoMetadata: GMSPlacePhotoMetadata) {
        GMSPlacesClient.shared().loadPlacePhoto(photoMetadata, callback: {
            (photo, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                self.restaurantImageView.image = photo;
                //self.attributionTextView.attributedText = photoMetadata.attributions;
            }
        })
    }
}
