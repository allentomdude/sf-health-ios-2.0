//
//  LocationSearchTable.swift
//  SF Health 2
//
//  Created by Allen Tom on 5/12/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift
import RealmMapView

class LocationSearchTable: UITableViewController {
    
    var mapView:RealmMapView? = nil
    var searchController: UISearchController? = nil
    
    var handleMapSearchDelegate:HandleMapSearch? = nil
    
    var restaurants:[RealmRestaurant] = []
    var realm: Realm!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let realmFile = Bundle.main.url(forResource: "default", withExtension: "realm")
        var realmConf = Realm.Configuration(fileURL: realmFile)
        realmConf.readOnly = true
        self.realm = try! Realm(configuration: realmConf)
        //self.restaurants = self.realm.objects(RealmRestaurant.self)
        
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        print("titleForHeaderInSection: \(section)")
        guard let searchController = self.searchController else {
            return "Error"
        }
        guard let searchText =  searchController.searchBar.text else {
            return "Error"
        }
        
        if searchText.characters.count < 1 {
            return "Recent Searches"
        }
        return "Matches"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("numberOfRowsInSection: \(section)")
        guard let searchController = self.searchController else {
            return 0
        }
        guard let searchText =  searchController.searchBar.text else {
            return 0
        }
        if searchText.characters.count < 1 {
            return SFHealth.savedSearches.count == 0 ? 1 : SFHealth.savedSearches.count
        }
        
        return self.restaurants.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let searchController = self.searchController else {
            return UITableViewCell()
        }
        guard let searchText =  searchController.searchBar.text else {
            return UITableViewCell()
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? RestaurantTableViewCell else {
            print("Couldn't dequeue RestaurantTableViewCell")
            return UITableViewCell()
        }
        
        if searchText.characters.count < 1 {
            if SFHealth.savedSearches.count > indexPath.row {
                let restaurant_id  = SFHealth.savedSearches[indexPath.row]
                if let r = realm.object(ofType: RealmRestaurant.self, forPrimaryKey: restaurant_id) {
                    cell.configureCell(restaurant: r)
                    return cell
                }
            }
            return UITableViewCell()
        }
        
        if restaurants.count > indexPath.row {
            let r = restaurants[indexPath.row]
            cell.configureCell(restaurant: r)
            return cell
        }
        else {
            print("cellForRowAt: \(indexPath.row) is greater than SFHealth.restaurants.count: \(SFHealth.restaurants.count)")
            return UITableViewCell()
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt: \(indexPath)")
        
        guard let searchController = self.searchController else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        guard let searchText =  searchController.searchBar.text else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        // need to tell MapVC to replace the navbar bar button items
        searchController.delegate?.didDismissSearchController!(searchController)

        
        //saved search
        if searchText.characters.count < 1 {
            if SFHealth.savedSearches.count > indexPath.row {
                let restaurant_id = SFHealth.savedSearches[indexPath.row]
                if let rest = realm.object(ofType: RealmRestaurant.self, forPrimaryKey: restaurant_id) {
                    handleMapSearchDelegate?.selectedRestaurant(restaurant: rest)
                }
                print ("Selected saved restaurant: \(restaurant_id)")
            }
            else {
                print("didSelectRowAt: savedSearches.count: \(SFHealth.savedSearches.count) < \(indexPath.row)")
            }
        }
            // regular search
            
        else if self.restaurants.count > indexPath.row {
            
            let rest = self.restaurants[indexPath.row]
            handleMapSearchDelegate?.selectedRestaurant(restaurant: rest)
            SFHealth.addSavedSearch(restaurant_id: rest.business_id)
        }
        else {
            print("didSelectRowAt: indexPath: \(indexPath), restaurants.count: \(SFHealth.restaurants.count)")
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
}

extension LocationSearchTable: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        print("updateSearchResults")
        self.searchController = searchController
        
        guard let mapView = mapView, let searchBarText = searchController.searchBar.text
            else {
                print("updateSearchResults: Error");
                return
        }
        
        
        if searchBarText.characters.count < 2 {
            tableView.isHidden = false
            tableView.reloadData()
            print("Need to type in more than 2 chars to searh")
            return
        }
        
        let restResult  = self.realm.objects(RealmRestaurant.self).filter("name CONTAINS[c] '\(searchBarText)'")
        
        self.restaurants = Array(restResult).sorted { (r1, r2) -> Bool in
            return r1.distanceMeters < r2.distanceMeters
        }
        
        
        let selectedIndexPath = tableView.indexPathForSelectedRow
        print("searchBarText: \(searchBarText) selectedIndexPath: \(selectedIndexPath as Optional)")
        
        // dismiss() will do one last call and we need to make sure that we don't make
        // another api call - otherwise, the annotation will be dismissed
        if selectedIndexPath == nil {
            self.tableView.reloadData()
            
        }
        
    }
}
