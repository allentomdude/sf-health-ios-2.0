//
//  AboutVC.swift
//  SF Health 2
//
//  Created by Allen Tom on 5/25/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import UIKit
import MessageUI

class InfoVC: UITableViewController, MFMailComposeViewControllerDelegate {
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("InfoVC: viewDidLoad")
        
        var versionStr = ""
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let buildVersionNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
        versionStr = "v:\(version) b:\(buildVersionNumber)"

        let versionLabel = UILabel(frame: CGRect(x: self.view.frame.width - 100, y: self.view.frame.height - 15 - 64, width: 100, height: 15))
        versionLabel.textAlignment = NSTextAlignment.right
        versionLabel.font = UIFont.systemFont(ofSize: 8, weight: UIFont.Weight.thin)
        versionLabel.textColor = UIColor.lightGray
        versionLabel.text = versionStr
        
        self.view.addSubview(versionLabel)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    let aboutIndexPath = IndexPath(row: 0, section: 0)
    let contactUsIndexPath = IndexPath(row: 0, section: 1)
    let shareIndexPath = IndexPath(row:0, section: 2)
    let rateIndexPath = IndexPath(row: 1, section: 2)
    let deleteHistoryIndexPath = IndexPath(row: 0, section:3)
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt: \(indexPath)")
        
        switch indexPath {
        case aboutIndexPath:
            print("About tapped")
            self.performSegue(withIdentifier: "showAboutSegue", sender: self)
        case contactUsIndexPath:
            print("Contact Us tapped")
            contactUs()
        case shareIndexPath:
            print("share tapped")
            share()
        case rateIndexPath:
            print("Rate tapped")
            rate()
        case deleteHistoryIndexPath:
            print("clear search history tapped")
            deleteHistory()
        default:
            print("Unknown indexPath tapped: \(indexPath)")
        }
        
    }
    
    func contactUs() {
        if MFMailComposeViewController.canSendMail() {
            let mailCompose = MFMailComposeViewController()
            mailCompose.setToRecipients(["atom@allentom.com"])
            mailCompose.setSubject("SF Health Feedback")
            mailCompose.mailComposeDelegate = self
            self.present(mailCompose, animated: true, completion: nil )
        }
        else {
            print ("Can't send mail")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    
    func share() {
        let appid = "1117187620"
        let shareText = "Checkout SF Health App - Restaurant Health Inspection Scores\n"
        let shareURL = NSURL(string: "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=\(appid)&mt=8")
        let activityVC = UIActivityViewController(activityItems: [shareText, shareURL!], applicationActivities: nil)
        
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone {
            self.present(activityVC, animated: true, completion: nil )
        }
        else {
            if let contentView = tableView.cellForRow(at: shareIndexPath)?.contentView {
                let tmpFrame = CGRect(x: contentView.frame.minX + 150, y: contentView.frame.midY, width: 1, height: 1)
                let tmpView = UIView(frame: tmpFrame)
                contentView.addSubview(tmpView)
                
                activityVC.modalPresentationStyle = UIModalPresentationStyle.popover
                activityVC.popoverPresentationController?.sourceView = tmpView
                
                self.present(activityVC, animated: true, completion: nil)
            }
        }
        
    }
    
    
    func rate() {
        let appid = "1117187620"
        let rateURL = URL(string: "itms-apps://itunes.apple.com/app/id\(appid)?action=write-review")!
        UIApplication.shared.open(rateURL,
                                  options: [:], completionHandler: nil)
    }

    
    func deleteHistory() {
        let alertController =  UIAlertController(title: "Do you really want to delete your search history?", message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            print("really deleting searches")
            SFHealth.deleteSavedSearches()
        }
        
        alertController.addAction(okAction)
        
        let laterAction = UIAlertAction(title: "Keep It", style: .cancel) { (action) in
            print("Not deleting seraches")
        }
        alertController.addAction(laterAction)
        
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
}
