//
//  MapVC.swift
//  SF Health 2
//
//  Created by Allen Tom on 5/12/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import UIKit
import MapKit
import RealmMapView
import RealmSwift
import ABFRealmMapView
import StoreKit


protocol HandleMapSearch {
    func selectedRestaurant(restaurant:RealmRestaurant)
}


class MapVC: UIViewController {
    
    
    @IBOutlet var customCalloutView: UIView!
    
    @IBOutlet var customCalloutRestaurantNameLabel: UILabel!
    
    @IBOutlet var customCalloutRestaurantAddressLabel: UILabel!
    
    @IBOutlet var customCalloutDistanceLabel: UILabel!
    @IBOutlet var filterBarView: FilterBarView!
    @IBOutlet var infoButton: UIBarButtonItem!
    
    
    @IBOutlet var listButton: UIBarButtonItem!
    @IBOutlet weak var mapView: RealmMapView!
    var locationManager = CLLocationManager()
    var resultSearchController:UISearchController? = nil
    var needsCenter = true
    // var location:CLLocation? = nil
    let annotationScoreTag = 1
    let annotationAddressTag = 2
    
    var selectedRestaurant:RealmRestaurant? = nil
    
    override func viewDidLoad() {
        let realmFile = Bundle.main.url(forResource: "default", withExtension: "realm")
        var realmConf = Realm.Configuration(fileURL: realmFile)
        realmConf.readOnly = true
        
        self.mapView.realmConfiguration = realmConf
        
        self.mapView.fetchedResultsController.clusterTitleFormatString = "$OBJECTSCOUNT restaurants in this area"
        self.mapView.maxZoomLevelForClustering = 16
        
        
        super.viewDidLoad()
        print("MapVC: viewDidLoad")
        
        mapView.delegate = self
        locationManager.delegate = self
        filterBarView.delegate = self
        
        //self.navigationItem.leftBarButtonItem = nil
        
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable
        resultSearchController?.delegate = self
        //resultSearchController?.searchBar.tintColor = UIColor.gray
        locationSearchTable.mapView = self.mapView
        locationSearchTable.handleMapSearchDelegate = self
        
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Search"
        searchBar.searchBarStyle = .prominent
        //   searchBar.keyboardAppearance = .dark
        navigationItem.titleView = resultSearchController?.searchBar
        
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        
        checkLocationAuthorizationStatus()
        
        mapView.tintColor = UIColor(red: 0x45/255.0 , green: 0x47/255.0, blue: 0xff/255.0, alpha: 1.0)
        
        // Set the cursor color in the search bar - the default is the tintColor, which is white and invisible
        if let subviews = resultSearchController?.searchBar.subviews.first?.subviews {
            for view in subviews {
                print("subview: \(view)")
                if view is UITextField {
                    view.tintColor = UIColor.gray
                }
            }
        }
        
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailVC = segue.destination as? DetailVC {
            if selectedRestaurant != nil {
                detailVC.restaurant = selectedRestaurant
                selectedRestaurant = nil
            }
                
            else if let a = mapView.selectedAnnotations.first as? ABFAnnotation {
                if let r = a.safeObjects.first?.rlmObject() as? RealmRestaurant {
                    detailVC.restaurant = r
                }
            }
        }
        
        if let listVC = segue.destination as? ListVC {
            listVC.mapView = self.mapView
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("MapVC: viewDidAppear")
        if #available(iOS 10.3, *) {
            if SFHealth.savedSearches.count > 3 {
                SKStoreReviewController.requestReview()
            }
        }
    }
    
    
    @IBAction func centerButtonTapped(_ sender: Any) {
        self.needsCenter = true // will automatically update the next time the user's location is updated
        
        if let coordinate = SFHealth.userLocation?.coordinate {
            let region = MKCoordinateRegionMakeWithDistance(coordinate, 640, 640)
            self.mapView.setRegion(region, animated: true)
        }
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("viewWillTransitionToSize: \(size)")
        filterBarView.didRotate(size: size)
    }
    
    func selectedSearchResult(r:RealmRestaurant) {
        print("MapvC: selectedSearchResult: \(r)")
    }
    
}

extension MapVC: filterBarViewDelegate {
    func scoreFilterUpdated() {
        print("MapVC: scoreFilterUpdated")
        
        // show everything
        if SFHealth.showHigh() && SFHealth.showOk() && SFHealth.showLow() {
            self.mapView.basePredicate = nil
        }
            
            // show low
        else if !SFHealth.showHigh() && !SFHealth.showOk() && SFHealth.showLow() {
            self.mapView.basePredicate = NSPredicate(format: "score < 80")
        }
            
            // show OK and Low
        else if !SFHealth.showHigh() && SFHealth.showOk() && SFHealth.showLow() {
            self.mapView.basePredicate = NSPredicate(format: "score < 90")
        }
            
            // show OK
        else if !SFHealth.showHigh() && SFHealth.showOk() && !SFHealth.showLow() {
            self.mapView.basePredicate = NSPredicate(format: "score >= 80 and score < 90")
        }
            
            // show High
        else if SFHealth.showHigh() && !SFHealth.showOk() && !SFHealth.showLow() {
            self.mapView.basePredicate = NSPredicate(format: "score >= 90")
        }
            
            // show High and OK
        else if SFHealth.showHigh() && SFHealth.showOk() && !SFHealth.showLow() {
            self.mapView.basePredicate = NSPredicate(format: "score >= 80")
        }
            
            // show High and Low
        else if SFHealth.showHigh() && !SFHealth.showOk() && SFHealth.showLow() {
            self.mapView.basePredicate = NSPredicate(format: "score >= 90 or score < 80")
        }
        
        
        self.mapView.refreshMapView()
        
    }
    
    func distanceFilterUpdated(distance:Double) {
        print("MapVC: DistanceFilterUpated distance: \(distance) meters")
        
        let currentCenter = mapView.centerCoordinate
        let region = MKCoordinateRegionMakeWithDistance(currentCenter, distance, distance)
        self.mapView.setRegion(region, animated: true)
        
    }
}


extension MapVC: UISearchControllerDelegate {
    func presentSearchController(_ searchController: UISearchController) {
        print("presentSearchController")
    }
    
    // Need to explicitly place the left and right bar button items whenever the 
    // search bar is dismissed
    
    func willPresentSearchController(_ searchController: UISearchController) {
        print("willPresentSearchController")
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        print("willDismissSearchController")
        self.navigationItem.leftBarButtonItem = infoButton
        self.navigationItem.rightBarButtonItem = listButton
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        print("MapVC: didDismissSearchController")
        self.navigationItem.leftBarButtonItem = infoButton
        self.navigationItem.rightBarButtonItem = listButton
        
    }
}


extension MapVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        print("regionWillChangeAnimated: there are \(self.mapView.annotations.count) annotations, safeObjects.count: \(self.mapView.fetchedResultsController.safeObjects.count)")
        //self.mapView.removeAnnotations(self.mapView.annotations)
    }
    
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        print("regionDidChangeAnimated, there are \(self.mapView.annotations.count) annotations")
        
        let centerCoord = mapView.centerCoordinate
        let centerLocation = CLLocation(latitude: centerCoord.latitude, longitude: centerCoord.longitude)
        
        let eastPoint = CGPoint(x: mapView.frame.size.width, y: mapView.frame.size.height / 2.0)
        
        let eastCoord = mapView.convert(eastPoint, toCoordinateFrom: mapView)
        let eastLocation = CLLocation(latitude: eastCoord.latitude, longitude: eastCoord.longitude)
        
        let radius = eastLocation.distance(from: centerLocation)
        
        print("radius: from \(centerCoord) to \(eastCoord) is \(radius) meters")
        
        
        
        let distanceMiles = radius / 1609.24
        let distanceMilesStr = String(format: "Distance  %.1f mi", distanceMiles)
        
        let distanceAttStr = NSAttributedString(string: distanceMilesStr,
                                                attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15),
                                                             NSAttributedStringKey.foregroundColor: UIColor.black])
        
        self.filterBarView.distanceButton.setAttributedTitle(distanceAttStr, for: .normal)
        
        
        let visibleMapRect = mapView.visibleMapRect
        
        let currentZoomLevel = ABFZoomLevelForVisibleMapRect(visibleMapRect)
        print("currentZoomLevel: \(currentZoomLevel)")
        
        
        
        //SFHealth.getRestaurants(map: mapView) { (s) in
        //    print("finished, there are \(SFHealth.restaurants.count) restaurants")
        //}
        
        var _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(self.selectAnnotation), userInfo: nil, repeats: false);
        
    }
    
    @objc func selectAnnotation() {
        if let annotationIDToSelect = SFHealth.annotationIDToSelect  {
            print("SelectAnnotation: looking to select \(annotationIDToSelect)")
            
            for case let a as ABFAnnotation in self.mapView.annotations  {
                if a.safeObjects.count == 1 {
                    if let rest =  a.safeObjects.first?.rlmObject() as? RealmRestaurant {
                        if rest.business_id == annotationIDToSelect {
                            SFHealth.annotationIDToSelect  = nil
                            self.mapView.selectAnnotation(a, animated: true)
                        }
                        
                    }
                }
                
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseidCluster = "cluster"
        let reuseidSingle = "single"
        guard let fetchedAnnotation = annotation as? ABFAnnotation else {
            print("XXXXX fetchedAnnotation isn't ABFnnotation!")
            return nil
        }
        if  UInt(fetchedAnnotation.safeObjects.count) > 1 {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseidCluster) as! ABFClusterAnnotationView?
            if annotationView == nil {
                annotationView = ABFClusterAnnotationView(annotation: fetchedAnnotation, reuseIdentifier: reuseidCluster )
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
                annotationView!.addGestureRecognizer(tapGesture)
                
                annotationView!.canShowCallout = true;
                annotationView!.color = UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
                annotationView!.detailCalloutAccessoryView = nil
            }
            annotationView!.count = UInt(fetchedAnnotation.safeObjects.count)
            annotationView!.annotation = fetchedAnnotation
            return annotationView
        }
        if  UInt(fetchedAnnotation.safeObjects.count) == 1 {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseidSingle)
            
            if annotationView == nil {
                //annotationView = ABFClusterAnnotationView(annotation: fetchedAnnotation, reuseIdentifier: reuseidSingle)
                annotationView = MKAnnotationView(annotation: fetchedAnnotation, reuseIdentifier: reuseidSingle)
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
                annotationView!.addGestureRecognizer(tapGesture)
                
                let score = UILabel(frame: CGRect(x: 2, y: 6, width: 30, height: 20))
                annotationView!.addSubview(score)
                score.tag = annotationScoreTag // needed to pull out the score later
                score.isHidden = true
                annotationView!.canShowCallout = true;
                annotationView!.detailCalloutAccessoryView = nil
            }
            
            
            if let a = fetchedAnnotation.safeObjects.first {
                if let r = a.rlmObject() as? RealmRestaurant {
                    
                    if let scoreLabel = annotationView!.viewWithTag(annotationScoreTag) as? UILabel  {
                        scoreLabel.isHidden = true
                        
                        
                        scoreLabel.text = "\(r.score)"
                        scoreLabel.font =  r.score == 100 ? UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold) : UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
                        scoreLabel.textColor = UIColor.white
                        scoreLabel.backgroundColor = UIColor.clear
                        scoreLabel.textAlignment = .center
                        
                        
                        if r.score >= 90 {
                            annotationView?.image = #imageLiteral(resourceName: "Location Indicator Green")
                            scoreLabel.textColor = UIColor.white
                        }
                        else if r.score >= 80 {
                            annotationView?.image = #imageLiteral(resourceName: "Location Indicator Yellow")
                            scoreLabel.textColor = UIColor(red: 0x4a/0xff, green: 0x4a/0xff, blue: 0x4a/0xff, alpha: 1.0)
                        }
                        else {
                            annotationView?.image = #imageLiteral(resourceName: "Location Indicator Red")
                            scoreLabel.textColor = UIColor.white
                        }
                        
                    }
                    let detailLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                    let distanceStr = SFHealth.distanceFromUserString(restaurant: r)
                    detailLabel.text = "\(a.subtitle)     \(distanceStr)"
                    detailLabel.font = UIFont.systemFont(ofSize: 15)
                    detailLabel.textColor = UIColor.gray
                    detailLabel.sizeToFit()
                    annotationView!.detailCalloutAccessoryView = detailLabel
                    return annotationView
                }
            }
            else {
                print("XXX more than 1 object")
            }
            
        }
        
        return nil
    }
    
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        print("tapped by \(sender)")
        guard let annotationView = sender.view as? MKAnnotationView else {
            print("not MKAnnodationView")
            return
        }
        guard let fetchedAnnotation = annotationView.annotation as? ABFAnnotation else {
            print("Not ABFAnnoation")
            return
        }
        if fetchedAnnotation.safeObjects.count != 1 {
            print("More than one safe object?")
            return
        }
        guard let rest = fetchedAnnotation.safeObjects.first?.rlmObject() as? RealmRestaurant else {
            print ("Couldn't cast to RealmRestaurant")
            return
        }
        if annotationView.isSelected {
            print("sender is an annotation: selected: \(annotationView.isSelected), name: \(rest.name), id: \(rest.business_id) ")
            self.performSegue(withIdentifier: "mapToDetailSegue", sender: self)
        }
        
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        guard let annotation = view.annotation as? ABFAnnotation else {
            return
        }
        if let scoreLabel = view.viewWithTag(annotationScoreTag) {
            scoreLabel.isHidden = true
        }
        
        if annotation.safeObjects.count == 1 {
            if let rest =  annotation.safeObjects.first?.rlmObject() as? RealmRestaurant {
                
                
                if rest.score >= 90 {
                    view.image = #imageLiteral(resourceName: "Location Indicator Green")
                }
                else if rest.score >= 80 {
                    view.image = #imageLiteral(resourceName: "Location Indicator Yellow")
                }
                else {
                    view.image = #imageLiteral(resourceName: "Location Indicator Red")
                }
                
            }
            
            print("Deselected \(annotation)")
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? ABFAnnotation else {
            return
        }
        
        
        if annotation.safeObjects.count == 1 {
            if let scoreLabel = view.viewWithTag(annotationScoreTag) {
                scoreLabel.isHidden = false
            }
            
            if let rest =  annotation.safeObjects.first?.rlmObject() as? RealmRestaurant {
                if rest.score >= 90 {
                    view.image = #imageLiteral(resourceName: "BigGreen")
                }
                else if rest.score >= 80 {
                    view.image = #imageLiteral(resourceName: "BigYellow")
                }
                else {
                    view.image = #imageLiteral(resourceName: "BigRed")
                }
                
            }
            
            print("Selected \(annotation)")
        }
    }
}

extension MapVC:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        
        SFHealth.userLocation = location
        // print("didUpdateLocations: location: \(location)")
        
        if needsCenter {
            //self.location = location
            /**let region = MKCoordinateRegion(center: location.coordinate,
             span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
             **/
            
            // 320 meters == 0.2 miles
            // 800 meters == 0.5 mi
            // 1600 meters == 1mi
            
            let region = MKCoordinateRegionMakeWithDistance(location.coordinate, 640, 640)
            self.mapView.setRegion(region, animated: true)
            
            needsCenter = false
        }
        
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
            locationManager.startUpdatingLocation()
        }
        else {
            locationManager.requestWhenInUseAuthorization()
            mapView.showsUserLocation = true
            locationManager.startUpdatingLocation()
        }
    }
}

extension MapVC: HandleMapSearch {
    func selectedRestaurant(restaurant r:RealmRestaurant) {
        print("MapVC selectedRestaurant: \(r)")
        self.selectedRestaurant = r
        SFHealth.annotationIDToSelect = r.business_id
        
        let restLocation = CLLocationCoordinate2D(latitude: r.latitude, longitude: r.longitude)
        let restRegion = MKCoordinateRegionMakeWithDistance(restLocation, 200, 200)
        self.mapView?.setRegion(restRegion, animated: true)
        
        self.performSegue(withIdentifier: "mapToDetailSegue", sender: self)
    }
    
    
}

