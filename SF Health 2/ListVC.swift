//
//  ListVC.swift
//  SF Health 2
//
//  Created by Allen Tom on 5/13/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import UIKit
import MapKit
import RealmMapView
import RealmSwift
import ABFRealmMapView


class ListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var mapView:RealmMapView? = nil
    
    var restaurants = [RealmRestaurant]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ListVC: viewDidLoad")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        guard let mapView = self.mapView else {
            print("No mapView")
            return
        }
        
        print("ListVC: there are \(mapView.annotations.count) annotations");
        
        for case let a as ABFAnnotation in mapView.annotations {
            for o in a.safeObjects {
                if let r = o.rlmObject() as? RealmRestaurant {
                    self.restaurants.append(r)
                }
            }
        }

        self.restaurants.sort(by: { (r1, r2) -> Bool in
                return r1.distanceMeters < r2.distanceMeters
        })
        
        
        print("ListVC: there are \(self.restaurants.count) restaurants")
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.restaurants.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt: \(indexPath)")
        self.performSegue(withIdentifier: "listToDetailSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? RestaurantTableViewCell else {
            print("Couldn't dequeue RestaurantTableViewCell")
            return UITableViewCell()
        }
        if indexPath.row > restaurants.count {
            print("Error: indexPath.row: \(indexPath.row) > restaurants.count: \(restaurants.count)")
            return UITableViewCell()
        }
        cell.configureCell(restaurant: self.restaurants[indexPath.row])
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailVC = segue.destination as? DetailVC {
            detailVC.restaurant = self.restaurants[(tableView.indexPathForSelectedRow?.row)!]
        }
    }
    
}

