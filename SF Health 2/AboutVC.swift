//
//  AboutVC.swift
//  SF Health 2
//
//  Created by Allen Tom on 6/6/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import UIKit

class AboutVC: UITableViewController {

    
    @IBOutlet var aboutTextLabel: UILabel!
    
    @IBOutlet var dataFromLabel: UILabel!
    
    
    @IBOutlet var howAreScoresCalculatedLabel: UILabel!
    
    @IBOutlet var howAreScoresCalculatedAnswer: UILabel!
    
    @IBOutlet var inspectionDetailsLabel: UILabel!
    
    @IBOutlet var inspectionDetailsAnswer: UILabel!
    
    
    @IBOutlet var reportIssuesLabel: UILabel!
    
    
    @IBOutlet var reportIssuesAnswer: UILabel!
    
    @IBOutlet var noScoreLabel: UILabel!
    
    @IBOutlet var noScoreAnswer: UILabel!
    
    @IBOutlet var cantFindLabel: UILabel!
    
    
    @IBOutlet var cantFindAnswer: UILabel!
    
    
    @IBOutlet var otherCitiesLabel: UILabel!
    
    @IBOutlet var otherCitiesAnswer: UILabel!
    
    
    @IBOutlet var resizableLabels: [UILabel]!
    
    
  
    @IBOutlet var sectionArrows: [UIImageView]!
    
    
    var aboutIndexPath = IndexPath(row: 0, section: 0)
    var scoresCalculatedIndexPath = IndexPath(row: 0, section: 1)
    var inspectionDetailsIndexPath = IndexPath(row: 1, section: 1)
    var reportIssuesIndexPath = IndexPath(row: 2, section: 1)
    var noScoreIndexPath = IndexPath(row: 3, section: 1)
    var cantFindIndexPath = IndexPath(row: 4, section: 1)
    var otherCitiesIndexPath = IndexPath(row: 5, section: 1)
    
    
    // height of expanded/collapsed cells
    var sectionHeights = [IndexPath:[String:CGFloat]]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("AboutVC: viewDidLoad")
        
                
        for l in resizableLabels {
            l.sizeToFit()
        }
        calculateHeights()
        
        
    
    }
    
    var expandedSection:IndexPath? = nil
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")
        tableView.reloadData()  // needed to resize first cell after layout
    }
    

    @IBAction func linkTapped(_ sender: Any) {
        print("linkTapped")
        UIApplication.shared.open(URL(string: "https://www.sfdph.org/")!, options: [:]) { (status) in
            print("Opened url")
        }
    }
    
    
    
    @IBAction func overviewOfRequirementsTapped(_ sender: Any) {
        if expandedSection == inspectionDetailsIndexPath {
            UIApplication.shared.open(URL(string: "https://www.sfdph.org/dph/EH/Food/Inspections.asp")!, options: [:]) { (status) in
                print("Opened url")
            }
        }
        else {
            print("section not expanded")
        }
    }
    
    
    @IBAction func sfdphWebsiteTapped(_ sender: Any) {
        if expandedSection == reportIssuesIndexPath {
            UIApplication.shared.open(URL(string: "https://www.sfdph.org/")!, options: [:]) { (status) in
                print("Opened url")
            }
        }
        else {
            print("section not expanded")
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print("heightForRowAt: \(indexPath)")
        
        if indexPath == expandedSection {
            return sectionHeights[indexPath]!["expanded"]!
        }
        else {
           return sectionHeights[indexPath]!["collapsed"]!
        }
        
        
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.medium)
        header.textLabel?.textColor = UIColor(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
    }
    
    func calculateHeights() {
        sectionHeights[aboutIndexPath] = [ "expanded": aboutTextLabel.frame.height + 250,
                                           "collapsed" : aboutTextLabel.frame.height + 250 ]
        
        
        sectionHeights[scoresCalculatedIndexPath] = [ "expanded":   73 + howAreScoresCalculatedAnswer.frame.height + 25,
                                                      "collapsed" :  73 ]

        sectionHeights[inspectionDetailsIndexPath] = [ "expanded":  73 + inspectionDetailsAnswer.frame.height + 25,
                                                       "collapsed" : 73 ]
        
        sectionHeights[reportIssuesIndexPath] = [ "expanded":   73 + reportIssuesAnswer.frame.height + 25,
                                                  "collapsed" : 73 ]
        
        sectionHeights[noScoreIndexPath] = [ "expanded":  73 + noScoreAnswer.frame.height + 25,
                                             "collapsed" : 73 ]
        
        sectionHeights[cantFindIndexPath] = [ "expanded":  73 + cantFindAnswer.frame.height + 25,
                                              "collapsed" : 73 ]
        
        sectionHeights[otherCitiesIndexPath] = [ "expanded":  73 + otherCitiesAnswer.frame.height + 25,
                                                 "collapsed" : 73 ]
        
    }
    
    func upArrow( indexPath: IndexPath ) {
        guard let contentView = tableView.cellForRow(at: indexPath)?.contentView else {
            print("Couldn't get contentView for \(indexPath)")
            return
        }
        
        
        
        for v in contentView.subviews {
            for sv in v.subviews {
            if let img = sv as? UIImageView  {
                print ("Uparrow animating arrow at indexPath: \(indexPath)")
                UIView.animate(withDuration: 0.2, animations: {
                    // it'll always take the shortest path, so -pi is the same as pi
                    img.transform = CGAffineTransform(rotationAngle: CGFloat.pi * -0.99999)
                })
            }
        }
        }
    }
    
    
 func downArrow( indexPath: IndexPath ) {
        guard let contentView = tableView.cellForRow(at: indexPath)?.contentView else {
            print("Couldn't get contentView for \(indexPath)")
            return
        }
        
    for v in contentView.subviews {
        for sv in v.subviews {
            if let img = sv as? UIImageView  {
                print ("Uparrow animating arrow at indexPath: \(indexPath)")
                UIView.animate(withDuration: 0.2, animations: {
                    img.transform = CGAffineTransform(rotationAngle: 0)
                })
            }
        }
    }

    
    
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt: \(indexPath)")
        
        if indexPath.section != 1 {
            return
        }
        
        self.tableView.beginUpdates()
        if expandedSection == indexPath {
            expandedSection = nil
        }
        else {
            expandedSection = indexPath
            upArrow(indexPath: indexPath)
        }
        
        for r in 0..<tableView.numberOfRows(inSection: 1) {
            let ip = IndexPath(row: r, section: 1)
            if ip != expandedSection {
                downArrow(indexPath: ip)
            }
        }
        
        self.tableView.endUpdates()


    }

    
    
}
