//
//  DetailVC.swift
//  SF Health 2
//
//  Created by Allen Tom on 5/17/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces
class DetailVC: UIViewController {
    
    var restaurant:RealmRestaurant? = nil
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var addressLabel: UILabel!
    
    
    @IBOutlet var scoreLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    
    
    @IBOutlet var mapView: MKMapView!
    
    @IBOutlet var scrollView: UIScrollView!
    
    
    @IBOutlet var gradientView: UIView!
    
    
    @IBOutlet var shareButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("DetailVC: viewDisLoad")
        guard let r = restaurant else {
            print("no restaurant!")
            return
        }
        
        self.mapView.delegate = self
        nameLabel.text = r.name
        addressLabel.text = r.address
        scoreLabel.text = "\(r.score)"
        
        scoreLabel.backgroundColor = SFHealth.scoreLabelColor(score: r.score)
        scoreLabel.textColor = SFHealth.scoreTextLabelColor(score: r.score)
        
        
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.clear.cgColor, UIColor(red: 0, green: 0, blue: 0, alpha: 0.85).cgColor]
        gradient.frame = gradientView.bounds
        gradientView.layer.insertSublayer(gradient, at: 0)
        
        
        distanceLabel.text = SFHealth.distanceFromUserString(restaurant: r)
        self.navigationItem.title = r.name
        let coord = CLLocationCoordinate2D(latitude: r.latitude, longitude: r.longitude)
        let region = MKCoordinateRegion(center: coord, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapView.setRegion(region, animated: true)
        
        let a = MKPointAnnotation()
        a.coordinate = CLLocationCoordinate2D(latitude: r.latitude, longitude: r.longitude)
        self.mapView.addAnnotation(a)
        
        if r.gplace_id != "" {
            loadFirstPhotoForPlace(placeID: r.gplace_id )
        }
        
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        
        var currentHeight = CGFloat(400.0)
        
        SFHealth.getRestaurantReports(id: r.business_id) { (combinedReports) in
            for r in combinedReports {
                print("DetailVC: combinedReport: \(r)")
                
                let reportView = RestaurantReportView(frame: CGRect(x: 0, y: Int(currentHeight),
                                                                    width: Int(self.scrollView.contentSize.width), height: 200))
                
                reportView.setup(report: r)
                currentHeight += (reportView.reportDetailsLabel.frame.height + 15)
                reportView.clipsToBounds = false
                
                self.scrollView.addSubview(reportView)
                
                print("scrollView contentsize is now: \(self.scrollView.contentSize)")
            }
            
            if currentHeight > self.scrollView.contentSize.height {
                self.scrollView.contentSize.height = currentHeight + 15
            }
            
        }
    }
    
    
    @IBAction func shareTapped(_ sender: Any) {
        print("shareTapped")
        guard let r = self.restaurant  else {
            print ("No restaurant!")
            return
        }
        
        let shareText = "\(r.name) has a \(r.score) health score.\nFind out more using the SF Health App"
        let shareURL = NSURL(string: "http://sfhealth.info/sfhealth/r/\(r.business_id)")
        let activityVC = UIActivityViewController(activityItems: [shareText, shareURL!], applicationActivities: nil)
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone {
            self.present(activityVC, animated: true, completion: nil )
        }
        else {
            activityVC.modalPresentationStyle = UIModalPresentationStyle.popover
            activityVC.popoverPresentationController?.barButtonItem  = shareButton
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}

extension DetailVC {
    func loadFirstPhotoForPlace(placeID: String) {
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: placeID) { (photos, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                if let firstPhoto = photos?.results.first {
                    self.loadImageForMetadata(photoMetadata: firstPhoto)
                }
            }
        }
    }
    
    func loadImageForMetadata(photoMetadata: GMSPlacePhotoMetadata) {
        GMSPlacesClient.shared().loadPlacePhoto(photoMetadata, callback: {
            (photo, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
            } else {
                self.imageView.image = photo;
                //self.attributionTextView.attributedText = photoMetadata.attributions;
            }
        })
    }
}

extension DetailVC: MKMapViewDelegate {
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let restaurant = self.restaurant else {
            return MKAnnotationView()
        }
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        let identifier = "pin"
        var view:MKAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }
        else {
            view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            
        }
        
        if restaurant.score >= 90 {
            view.image = #imageLiteral(resourceName: "Location Indicator Green")
        }
        else if restaurant.score >= 80 {
            view.image = #imageLiteral(resourceName: "Location Indicator Yellow")
        }
        else {
            view.image = #imageLiteral(resourceName: "Location Indicator Red")
        }
        
        return view
        
        
        
    }
}
