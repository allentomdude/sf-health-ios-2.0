//
//  SFHealthData.swift
//  SF Health 2
//
//  Created by Allen Tom on 5/12/17.
//  Copyright © 2017 Allen Tom. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation
import MapKit
import RealmSwift

class RealmRestaurant: Object {
    @objc dynamic var name = ""
    @objc dynamic var address = ""
    @objc dynamic var business_id = 0
    @objc dynamic var gplace_id = ""
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var score = 0
    override public static func primaryKey() -> String? {
        return "business_id"
    }
    
    @objc dynamic var distanceMeters:CLLocationDistance {
        guard let userLocation = SFHealth.userLocation else {
            return 999
        }
        let restLocation = CLLocation(latitude: self.latitude, longitude: self.longitude)
        return userLocation.distance(from: restLocation)
    }
    
    override static func ignoredProperties() -> [String] {
        return ["distanceMeters"]
    }
}

/* SFHealthRealmSetup: only used to generate RealmDB in simulator */
class SFHealthRealmSetup {
    static func loadRealmDB(completion: @escaping() -> Void) {
        var config = Realm.Configuration.defaultConfiguration
        
        let realm = try! Realm()

        try! realm.write {
            realm.deleteAll()
        }
        

        print("fileURL: \(config.fileURL as Optional)")
        let endpoint:String  =  "\(SFHealth.server)/sfhealth?json=on"
        getData(url: endpoint, completion: completion)
    }
    
    static func getData(url:String, completion: @escaping() -> Void) {
        let realm = try! Realm()
     

        
        Alamofire.request( url, parameters: nil).responseJSON { (response) in
            print(response.request ?? "No request?")
            print(response.response ?? "no response?")
            print(response.data ?? "no data?")
            print(response.result)
            
            var nextURLString:String? = nil

            if let result = response.result.value {
                
                let JSON = result as! NSDictionary

                if let next = JSON["next"] as? String {
                    nextURLString = next
                }
                
                if let results = JSON["results"] as? NSArray {
                    for r in results {
                        if let rest = parseRealmRestaurant(r as! NSDictionary) {
                            print("Realm Restaurant: \(rest)")
                            
                            try! realm.write() {
                                realm.add(rest)
                            }
                        }
                    }
                }
            }
            if nextURLString != nil {
                getData(url: nextURLString!, completion: completion)
            }
            else {
                let defaultURL = Realm.Configuration.defaultConfiguration.fileURL!
                let newURL = URL(string: "\(defaultURL).saved")
                try! realm.writeCopy(toFile: newURL!)
                completion()
            }
        }
    }
    
    
    
    
    static func parseRealmRestaurant(_ result: NSDictionary) -> RealmRestaurant? {
        guard let name = result["name"] as? String else {
            return nil
        }
        guard let id = result["business_id"] as? Int else {
            return nil
        }
        guard let address = result["business_address"] as? String else {
            return nil
        }
        guard let score = result["inspection_score"] as? Int else {
            return nil
        }
        guard let lat = result["lat"] as? Double else {
            return nil
        }
        guard let lon = result["lon"] as? Double else {
            return nil
        }
        
        
        let place_id = result["gplace_id"] as? String ?? nil
        
        let rv = RealmRestaurant()
        rv.business_id = id
        rv.name = name
        rv.address = address
        rv.latitude = lat
        rv.longitude = lon
        rv.score = score
        if place_id != nil {
            rv.gplace_id = place_id!
        }
        return rv
    }
    
    
}

class Restaurant : NSObject, MKAnnotation, NSCoding  {
    var score = 0
    var name: String
    var address: String
    var business_id = 0
    var gplace_id: String?
    var coordinate:CLLocationCoordinate2D
    
    static func labelColor(score:Int) -> UIColor  {
        if score >= 90 {
            return UIColor(red: 59.0/255.0, green: 183.0/255.0, blue: 117.0/255.0, alpha: 1.0)
        }
        else if score >= 80 {
            return UIColor(red: 241.0/255.0, green: 191.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        }
        else {
            return UIColor(red: 233.0/255.0, green: 50.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        }
    }
    
    static func textLabelColor(score: Int) -> UIColor {
        if score >= 80 && score < 90 {
            return UIColor(red: 0x4a/0xff, green: 0x4a/0xff, blue: 0x4a/0xff, alpha: 1.0)
        }
        else {
            return UIColor.white
        }
    }
    
    var scoreLabelColor:UIColor {
        return Restaurant.labelColor(score: self.score)
    }
    
    var scoreLabelTextColor: UIColor {
        return Restaurant.textLabelColor(score: self.score)
    }
    
    func distance(from:CLLocation) -> CLLocationDistance {
        let coord = CLLocation(latitude: self.coordinate.latitude, longitude: self.coordinate.longitude)
        return from.distance(from: coord)
    }
    
    func distanceFromUserMeters() -> Double {
        guard let userLocation = SFHealth.userLocation else {
            return 999.9
        }
        
        return self.distance(from: userLocation)
        
    }
    
    func distanceFromUserStr() -> String {
        guard let userLocation = SFHealth.userLocation else {
            return "unknown"
        }
        
        let distanceMeters = self.distance(from: userLocation)
        let distanceMiles = distanceMeters/1609.34
        
        return String(format: "%.2f mi", distanceMiles)
        
    }
    
    var title:String?  {
        return self.name
    }
    var subtitle: String? {
        return self.address
    }
    
    // needed for Array.contains()
    override func isEqual(_ object: Any?) -> Bool {
        if let other = object as? Restaurant {
            return self.business_id == other.business_id
        }
        else {
            return false
        }
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encodeCInt(Int32(business_id), forKey: "business_id")
        aCoder.encode(score, forKey: "score")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(gplace_id, forKey: "gplace_id")
        
        // can't encode CLLocationCoordinate2D
        let lat = coordinate.latitude as Double
        let lon = coordinate.longitude as Double
        aCoder.encode(lat, forKey: "lat")
        aCoder.encode(lon, forKey: "lon")
        
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        
        let business_id = aDecoder.decodeInteger(forKey: "business_id")
        let score = aDecoder.decodeInteger(forKey: "score")
        let name  = aDecoder.decodeObject(forKey: "name") as!  String
        let address = aDecoder.decodeObject(forKey: "address") as! String
        let gplace_id = aDecoder.decodeObject(forKey: "gplace_id") as! String?
        
        let lat = aDecoder.decodeDouble(forKey: "lat" )
        let lon = aDecoder.decodeDouble(forKey: "lon")
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        
        self.init(business_id: business_id, name: name, address: address, score: score, gplace_id: gplace_id, coordinate: coordinate)
    }
    
    
    
    
    init(business_id:Int, name: String, address: String, score: Int, gplace_id: String?, coordinate: CLLocationCoordinate2D) {
        self.business_id = business_id
        self.name = name
        self.address = address
        self.score = score
        self.gplace_id = gplace_id
        self.coordinate = coordinate
    }
    
    override var description: String {
        return "Restaurant: id:\(business_id) , name:\(name), score:\(score), address:\(address), gplace_id:\(gplace_id as Optional), coordiante:\(coordinate)"
    }
}


class RestaurantReport: NSObject {
    var date: Date
    var score: Int
    var violation_description: String
    var risk_category: String
    
    init( date: Date, score: Int, violation_description: String, risk_category: String) {
        self.date = date
        self.score = score
        self.violation_description = violation_description
        self.risk_category = risk_category
    }
    
    override var description: String {
        return "Report: date: \(date), score: \(score), violation_description: \(violation_description), risk_category: \(risk_category)"
    }
}


class CombinedRestaurantReport: NSObject {
    var date: Date
    var score: Int
    var descriptions: [String]
    init(date: Date, score: Int, descriptions:[String]) {
        self.date = date
        self.score = score
        self.descriptions = descriptions
    }
    override var description:String {
        return "CombinedRestaurantReport: date\(date), score: \(score), descriptions: \(descriptions)"
    }
}


enum ScoreFilter {
    case low, ok, high
}

class SFHealth {
    static let MAX_RESTAURANTS = 400 // no more than this!
    static var annotationIDToSelect:Int? = nil // used to select annotation when mapview loads after selecting List item
    
    // static var scoreFilters = Set(arrayLiteral: ScoreFilter.low, ScoreFilter.ok, ScoreFilter.high)
    static var scoreFilters = Set<ScoreFilter>()
    
    static var restaurants:[Restaurant] = []
    static var savedSearches:[Int] = []
    
    
    //static let server = "http://13.57.223.92:8080" // Django EC2
    //static let server = "http://localhost:5000" //firebase
    static let server = "https://sfhealth.info"
    
    static var userLocation:CLLocation? = nil
    
    static func scoreLabelColor(score:Int) -> UIColor  {
        if score >= 90 {
            return UIColor(red: 59.0/255.0, green: 183.0/255.0, blue: 117.0/255.0, alpha: 1.0)
        }
        else if score >= 80 {
            return UIColor(red: 241.0/255.0, green: 191.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        }
        else {
            return UIColor(red: 233.0/255.0, green: 50.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        }
    }
    
    static func scoreTextLabelColor(score: Int) -> UIColor {
        if score >= 80 && score < 90 {
            return UIColor(red: 0x4a/0xff, green: 0x4a/0xff, blue: 0x4a/0xff, alpha: 1.0)
        }
        else {
            return UIColor.white
        }
    }

    
    static func distanceFromUserString(restaurant: RealmRestaurant) -> String {
        guard let userLocation = userLocation else {
            return ""
        }
        let restLocation = CLLocation(latitude: restaurant.latitude, longitude: restaurant.longitude)
        let distanceMeters = userLocation.distance(from: restLocation)
        let distanceMiles = distanceMeters/1609.34
        
        return String(format: "%.2f mi", distanceMiles)
    }
    
    
    static func getRestaurantReports( id:Int, completion: @escaping([CombinedRestaurantReport]) ->Void ) {
        let endpoint = "\(server)/sfhealth/business/\(id)/detail"
        
        Alamofire.request(endpoint).responseJSON { (response) in
            print(response.request!)
            print(response.response ?? "no response")
            print(response.data ?? "no data?")
            print(response.result)
            
            // the server returns line item in an inspection as a separate report, but
            // we want to group them all together by date into a CombinedReport
            var parsedReports = [RestaurantReport]()
            var combinedReports = [CombinedRestaurantReport]()
            var currentCombinedReport:CombinedRestaurantReport? = nil
            
            if let result = response.result.value {
                
                let JSON = result as! NSDictionary
                if let reports = JSON["reports"] as? NSArray {
                    for r in reports {
                        if let r = parseReport(r as! NSDictionary) {
                            parsedReports.append(r)
                            if currentCombinedReport == nil {
                                currentCombinedReport = CombinedRestaurantReport(date: r.date,
                                                                                 score: r.score,
                                                                                 descriptions: [r.violation_description])
                            }
                            else if currentCombinedReport!.date == r.date {
                                currentCombinedReport?.descriptions.append(r.violation_description)
                            }
                            else {
                                combinedReports.append(currentCombinedReport!)
                                currentCombinedReport = CombinedRestaurantReport(date: r.date,
                                                                                 score: r.score,
                                                                                 descriptions: [r.violation_description])
                            }
                        }
                    }
                }
            }
            
            if currentCombinedReport != nil {
                combinedReports.append(currentCombinedReport!)
            }
            
            completion(combinedReports)
        }
    }
    
    static func addSavedSearch(restaurant_id:Int) {
        if SFHealth.savedSearches.contains(restaurant_id) == false {
            SFHealth.savedSearches.insert(restaurant_id, at: 0)
            let userDefaults = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: SFHealth.savedSearches)
            userDefaults.set(encodedData, forKey: "savedSearchesIds")
            userDefaults.synchronize()
        }
        
    }
    
    static func deleteSavedSearches() {
        SFHealth.savedSearches.removeAll()
        let userDefaults = UserDefaults.standard
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: SFHealth.savedSearches)
        userDefaults.set(encodedData, forKey: "savedSearches")
        userDefaults.synchronize()
    }
    
    static func loadSavedSearches() {
        let userDefaults = UserDefaults.standard
        if let decoded = userDefaults.object(forKey: "savedSearchesIds") as? Data {
            print("loading savedSearches from userDefaults")
            do {
                SFHealth.savedSearches = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as! [Int]
                print("loaded saved searches: \(SFHealth.savedSearches.count) saved searches")
            } catch let error {
                print("Caught error \(error.localizedDescription) while loading savedSearches")
            }
        }
    }
    
    static func showLow() -> Bool {
        return scoreFilters.contains(.low) || scoreFilters ==  Set(arrayLiteral: ScoreFilter.low, ScoreFilter.ok, ScoreFilter.high) || scoreFilters == Set<ScoreFilter>()
    }
    
    static func showOk() -> Bool {
        return scoreFilters.contains(.ok) || scoreFilters ==  Set(arrayLiteral: ScoreFilter.low, ScoreFilter.ok, ScoreFilter.high) || scoreFilters == Set<ScoreFilter>()
    }
    
    static func showHigh() -> Bool {
        return scoreFilters.contains(.high) || scoreFilters ==  Set(arrayLiteral: ScoreFilter.low, ScoreFilter.ok, ScoreFilter.high) || scoreFilters == Set<ScoreFilter>()
    }
    
    
    
    static func getRestaurants(search:String, map:MKMapView, completion: @escaping([String]) -> Void) {
        let  userLocation = map.userLocation
        let  result_sort = "distance"
        
        if search.characters.count < 2 {
            completion(["too short"])
            return
        }
        
        
        let endpoint =  "\(server)/sfhealth"
        
        let params = [  "query": search,
                        "userlocation": "\(userLocation.coordinate.latitude),\(userLocation.coordinate.longitude)",
            "sort": result_sort,
            "json": "on" ]
        
        SFHealth.restaurants.removeAll()
        map.removeAnnotations(map.annotations)
        
        Alamofire.request( endpoint, parameters: params).responseJSON { (response) in
            print(response.request!)
            print(response.response ?? "no response?")
            print(response.data ?? "no data?")
            print(response.result)
            
            var nextURL:URL? = nil
            
            
            
            if let result = response.result.value {
                
                let JSON = result as! NSDictionary
                if let next = JSON["next"] as? String {
                    nextURL = URL(string: next)
                }
                
                let count = JSON["count"] as? Int
                
                if let results = JSON["results"] as? NSArray {
                    for r in results {
                        if let rest = parseResult(r as! NSDictionary) {
                            
                            if rest.score < 80 && SFHealth.showLow() {
                                SFHealth.addRestaurant(restaurant: rest, map: map)
                            }
                            else if rest.score < 90 &&  rest.score >= 80 && SFHealth.showOk() {
                                SFHealth.addRestaurant(restaurant: rest, map: map)
                            }
                            else if rest.score >= 90 && SFHealth.showHigh() {
                                SFHealth.addRestaurant(restaurant: rest, map: map)
                            }
                            else {
                                //print("Dropping because doesn't match search filter: \(rest)")
                            }
                        }
                    }
                }
            }
            
            
            if nextURL != nil {
                completion(["in proggress"])
                if SFHealth.restaurants.count <= SFHealth.MAX_RESTAURANTS {
                    getNextRestaurants(map: map, next: nextURL!, completion: completion)
                }
                completion(["too many restaurants"])
            }
            else {
                completion(["hello"])
            }
        }
        
    }
    
    
    
    
    
    static func getRestaurants(map: MKMapView, completion: @escaping([String]) -> Void ) {
        
        //map.removeAnnotations(map.annotations)
        //restaurants.removeAll()
        
        // remove annotations that aren't visible
        
        if let visibleRestaurants  = map.annotations(in: map.visibleMapRect) as? Set<Restaurant> {
            
            let invisibleRestaurants = Set(restaurants).subtracting(visibleRestaurants)
            restaurants = Array(visibleRestaurants)
            map.removeAnnotations(Array(invisibleRestaurants))
            
        }
        
        
        let  edges = map.edgePoints()
        let  userLocation = map.userLocation
        let  result_sort = "distance"
        
        let endpoint =  "\(server)/sfhealth"
        
        let params = [  "query": "",
                        "northeast": "\(edges.ne.latitude),\(edges.ne.longitude)",
            "southwest": "\(edges.sw.latitude),\(edges.sw.longitude)",
            "userlocation": "\(userLocation.coordinate.latitude),\(userLocation.coordinate.longitude)",
            "sort": result_sort,
            "json": "on" ]
        
        Alamofire.request( endpoint, parameters: params).responseJSON { (response) in
            print(response.request!)
            print(response.response ?? "no response?")
            print(response.data ?? "no data?")
            print(response.result)
            
            var nextURL:URL? = nil
            
            guard let requestURL = response.request?.url else {
                print("Missing requestURL!")
                return
            }
            
            let currentEdges = map.edgePoints()
            let urlComponents = URLComponents(url: requestURL, resolvingAgainstBaseURL: false)
            
            let requestNE = urlComponents?.queryItems?.first(where: { $0.name == "northeast" })?.value
            
            let requestSW = urlComponents?.queryItems?.first(where: { $0.name == "southwest" })?.value
            
            
            if requestNE != nil && requestSW != nil {
                
                if requestNE == "\(currentEdges.ne.latitude),\(currentEdges.ne.longitude)" &&
                    requestSW == "\(edges.sw.latitude),\(edges.sw.longitude)" {
                    print("Response matches current map position!")
                    completion(["done"])
                }
                else {
                    print("Response doesn't match current map position! :(")
                    completion(["done"])
                }
            }
            
            
            if let result = response.result.value {
                
                let JSON = result as! NSDictionary
                if let next = JSON["next"] as? String {
                    nextURL = URL(string: next)
                }
                
                if let results = JSON["results"] as? NSArray {
                    for r in results {
                        if let rest = parseResult(r as! NSDictionary) {
                            
                            if rest.score < 80 && SFHealth.showLow() {
                                SFHealth.addRestaurant(restaurant: rest, map: map)
                            }
                            else if rest.score < 90 && rest.score >= 80 && SFHealth.showOk() {
                                SFHealth.addRestaurant(restaurant: rest, map: map)
                            }
                            else if rest.score >= 90 && SFHealth.showHigh() {
                                SFHealth.addRestaurant(restaurant: rest, map: map)
                            }
                            else {
                                //print("Dropping because doesn't match search filter: \(rest)")
                            }
                        }
                    }
                }
            }
            
            
            if nextURL != nil {
                getNextRestaurants(map: map, next: nextURL!, completion: completion)
            }
            else {
                completion(["hello"])
            }
        }
        
    }
    
    static func addRestaurant(restaurant: Restaurant, map: MKMapView) {
        if restaurants.contains(restaurant) == false {
            print("addRestaurant: adding: \(restaurant)")
            SFHealth.restaurants.append(restaurant)
            map.addAnnotation(restaurant)
        }
    }
    
    
    static func getNextRestaurants(map:MKMapView, next: URL, completion: @escaping([String]) -> Void ) {
        let  edges = map.edgePoints()
        
        Alamofire.request( next, parameters: nil).responseJSON { (response) in
            print(response.request!)
            print(response.response ?? "no response?")
            print(response.data ?? "no data?")
            print(response.result)
            
            var nextURL:URL? = nil
            
            guard let requestURL = response.request?.url else {
                print("Missing requestURL!")
                return
            }
            
            let currentEdges = map.edgePoints()
            let urlComponents = URLComponents(url: requestURL, resolvingAgainstBaseURL: false)
            
            
            let requestNE = urlComponents?.queryItems?.first(where: { $0.name == "northeast" })?.value
            
            let requestSW = urlComponents?.queryItems?.first(where: { $0.name == "southwest" })?.value
            
            
            if requestNE != nil && requestSW != nil {
                
                if requestNE == "\(currentEdges.ne.latitude),\(currentEdges.ne.longitude)" &&
                    requestSW == "\(edges.sw.latitude),\(edges.sw.longitude)" {
                    print("Response matches current map position!")
                }
                else {
                    print("Response doesn't match current map position! :(")
                    completion(["done"])
                    return
                }
            }
            
            
            if let result = response.result.value {
                
                let JSON = result as! NSDictionary
                if let next = JSON["next"] as? String {
                    nextURL = URL(string: next)
                }
                
                if let results = JSON["results"] as? NSArray {
                    for r in results {
                        if let rest = parseResult(r as! NSDictionary) {
                            
                            if rest.score < 80 && SFHealth.showLow() {
                                SFHealth.addRestaurant(restaurant: rest, map: map)
                            }
                            else if rest.score < 90 && rest.score >= 80 && SFHealth.showOk() {
                                SFHealth.addRestaurant(restaurant: rest, map: map)
                            }
                            else if rest.score >= 90 && SFHealth.showHigh() {
                                SFHealth.addRestaurant(restaurant: rest, map: map)
                            }
                            else {
                                //print("Dropping because doesn't match search filter: \(rest)")
                            }
                        }
                    }
                }
            }
            
            
            if nextURL != nil {
                getNextRestaurants(map: map, next: nextURL!, completion: completion)
            }
            else {
                completion(["hello"])
            }
        }
        
        
        
        
        
    }
    
    
    static func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    static func parseReport(_ report: NSDictionary) -> RestaurantReport? {
        guard let inspection_date_str = report["inspection_date"] as? String else {
            return nil
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        guard let inspection_date = dateFormatter.date(from: inspection_date_str) else {
            return nil
        }
        
        guard let inspection_score = report["inspection_score"] as? Int else {
            return nil
        }
        guard let violation_description = report["violation_description"] as? String else {
            return nil
        }
        guard let risk_category = report["risk_category"] as? String else {
            return nil
        }
        
        return RestaurantReport(date: inspection_date, score: inspection_score,
                                violation_description: violation_description,
                                risk_category: risk_category)
        
    }
    
    static func parseResult(_ result: NSDictionary) -> Restaurant? {
        guard let name = result["name"] as? String else {
            return nil
        }
        guard let id = result["business_id"] as? Int else {
            return nil
        }
        guard let address = result["business_address"] as? String else {
            return nil
        }
        guard let score = result["inspection_score"] as? Int else {
            return nil
        }
        guard let lat = result["lat"] as? Double else {
            return nil
        }
        guard let lon = result["lon"] as? Double else {
            return nil
        }
        
        
        let place_id = result["gplace_id"] as? String ?? nil
        
        let coords = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        return Restaurant(business_id: id, name: name, address: address, score: score, gplace_id: place_id, coordinate: coords)
        
    }
    
}

extension MKMapView {
    typealias Edges = (ne: CLLocationCoordinate2D, sw: CLLocationCoordinate2D)
    
    func edgePoints() -> Edges {
        let nePoint = CGPoint(x: self.bounds.maxX, y: self.bounds.origin.y)
        let swPoint = CGPoint(x: self.bounds.minX, y: self.bounds.maxY)
        let neCoord = self.convert(nePoint, toCoordinateFrom: self)
        let swCoord = self.convert(swPoint, toCoordinateFrom: self)
        return (ne: neCoord, sw: swCoord)
    }
    
}
