#!/bin/bash
# get the list of business ids using the Django shell and sve it to business_ids.txt

DJANGO=http://13.57.223.92:8080
while read -r line
do
   id="$line"
   `/usr/bin/curl  -O  $DJANGO/sfhealth/r/$id`
    `/usr/bin/curl --create-dirs -o  ../business/$id/detail $DJANGO/sfhealth/business/$id/detail`
   echo "Saved business_id $id"
done < "business_ids.txt"
